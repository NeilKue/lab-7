/*********************************************
Name: Neil Kuehn
Class & Section: CPSC 1021, Lab Section 4, F20
Email: nckuehn@clemson.edu
Section TAs: Elliot McMillan & Matt Franchi
*********************************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {

  /* This is to seed the random generator*/
  srand(unsigned (time(0)));

	employee array[10];// creates the array array
	//gets all of the data for the varialbes inside the structs
	for (int i = 0; i < 10; i++){
		cout << "Enter the information for employee #" << (i+1) << "\n" << endl;
		cout << "First Name: ";
		cin >> array[i].firstName;
		cout << "Last Name: ";
		cin >> array[i].lastName;
		cout << "Birth Year: ";
		cin >> array[i].birthYear;
		cout << "Hourly Wage: $";
		cin >> array[i].hourlyWage;
		cout << endl;
	}

	 employee *startPointer = array;// pointer to the beginning of array1
	 employee *endPointer = array + 10;//pointer to one past the end of array1
	 random_shuffle(startPointer, endPointer, myrandom);
	 //random_shuffle randomly suffles the array array

		//creates an array with values from the first array after it is rondomized
		employee array2[5] = {array[0], array[1], array[2], array[3], array[4]};


		employee *startPointer2 = array2;//pointer to the beginning of array2
		employee *endPointer2 = array2 + 5;//pointer to one past the end of array2
		sort(startPointer2, endPointer2, name_order);
		//sorts the array2

		// this loop prints out
		for (int i = 0; i < 5; i++){
			cout << array2[i].lastName << ", " << array2[i].firstName << endl;
			cout << array2[i].birthYear << endl;
			cout << "$"<< showpoint << setprecision(3) << array2[i].hourlyWage << "\n\n" << endl;
		}


  return 0;
}


/*this funciton tell the sort function when to sort the different elements
 in the array2 array. It would tell it to sort decending by last name
 then firstname.*/
bool name_order(const employee& lhs, const employee& rhs) {
	if ((lhs.lastName).compare(rhs.lastName) < 0){
		return true;
	}else if ((rhs.lastName).compare(lhs.lastName) < 0) {
		return false;
	} else if((lhs.firstName).compare(rhs.firstName) < 0) {
		return true;
	} else if((rhs.firstName).compare(lhs.firstName) < 0) {
		return false;
	} else {
		return false;
	}
}
